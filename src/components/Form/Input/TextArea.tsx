import {
  FormControlProps,
  TextareaAutosize,
  TextareaAutosizeProps,
} from "@mui/material";
import type { FieldValues, UseControllerProps } from "react-hook-form";
import { useController } from "react-hook-form";
import type { AddControlProps } from "./InputControl";
import { InputControl } from "./InputControl";
import { base } from "../../../libs/config/theme";

export type BaseInputProps<T extends FieldValues> = UseControllerProps<T> &
  AddControlProps & {
    controlProps?: FormControlProps;
    width?: string;
    labelLeft?: boolean;
    padding?: string;
    labelHeight?: number | string;
    fullWidth?: boolean;
  };

export type TextAreaProps<T extends FieldValues> = BaseInputProps<T> &
  TextareaAutosizeProps;

function TextArea<T extends FieldValues>({
  name,
  control,
  defaultValue,
  fullWidth,
  label,
  labelRight,
  helperText,
  controlProps,
  width = "100%",
  padding,
  required,
  labelLeft,
  labelHeight,
  ...props
}: TextAreaProps<T>) {
  const {
    field: { ref, ...inputProps },
    fieldState: { error },
  } = useController({ name, control, defaultValue });

  return (
    <InputControl
      fieldError={error}
      fullWidth={fullWidth}
      label={label}
      required={required}
      labelLeft={labelLeft}
      helperText={helperText}
      labelRight={labelRight}
      labelHeight={labelHeight}
      disabled={props.disabled}
      {...controlProps}
    >
      <TextareaAutosize
        {...inputProps}
        {...props}
        ref={ref}
        style={{
          width: width,
          height: "100px",
          padding: padding,
          border: `2px solid ${base.violet}`,
          borderRadius: "4px",
          borderColor: base.bg_violet,
          backgroundColor: base.bg_violet,
          outlineColor: base.violet,
        }}
        onFocus={(e) => {
          e.target.style.borderColor = base.bg_violet;
        }}
        onBlur={(e) => {
          e.target.style.borderColor = base.bg_violet;
        }}
      />
    </InputControl>
  );
}

export default TextArea;
