import {
  FormControlLabel,
  FormControlProps,
  OutlinedInputProps,
  Radio,
  RadioGroup,
} from "@mui/material";
import {
  FieldValues,
  UseControllerProps,
  useController,
} from "react-hook-form";
import { AddControlProps, InputControl } from "../Input/InputControl";

export type RadioProps<T extends FieldValues> = UseControllerProps<T> &
  OutlinedInputProps &
  AddControlProps & {
    controlProps?: FormControlProps;
    labelInline?: boolean;
  } & { options: Array<{ value: number | string; label: string }> };

function RadioComponent<T extends FieldValues>({
  name,
  control,
  defaultValue,
  fullWidth,
  label,
  helperText,
  controlProps,
  options,
  disabled,
  labelInline,
}: RadioProps<T>) {
  const {
    field: { ...inputProps },
    fieldState: { error },
  } = useController({ name, control, defaultValue });

  return (
    <InputControl
      fieldError={error}
      fullWidth={fullWidth}
      label={label}
      helperText={helperText}
      disabled={disabled}
      {...controlProps}
      labelInline={labelInline}
    >
      <RadioGroup
        {...inputProps}
        row
        aria-labelledby="demo-row-radio-buttons-group-label"
        name="row-radio-buttons-group"
      >
        {options.map((o: { value: number | string; label: string }, index) => {
          return (
            <FormControlLabel
              value={o.value}
              control={<Radio />}
              label={o.label}
              key={index}
            />
          );
        })}
      </RadioGroup>
    </InputControl>
  );
}

export { RadioComponent };
