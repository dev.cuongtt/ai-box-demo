import {
  Box,
  Button,
  Grid,
  styled,
  CircularProgress,
  Typography,
} from "@mui/material";
import { useState } from "react";
import { SubmitHandler, useForm } from "react-hook-form";
import { RadioComponent } from "../components/Form";
import { yupResolver } from "@hookform/resolvers/yup";
import { Demo, DemoSchema, useDemoCreate } from "./hooks";
import TextArea from "../components/Form/Input/TextArea";

const SIZE = [
  { label: "1024x1024", value: "1024x1024" },
  { label: "512x512", value: "512x512" },
];

const DemoScreen = () => {
  const [url, setUrl] = useState("");
  const [loading, setLoading] = useState(false);
  const { control, handleSubmit, setError } = useForm<Demo>({
    defaultValues: {
      prompt: "",
      size: "",
    },
    resolver: yupResolver(DemoSchema),
  });

  const { mutate: createDemo } = useDemoCreate(setError);

  const onSubmit: SubmitHandler<Demo> = (data) => {
    console.log(data);

    setLoading(true);
    createDemo(data, {
      onSuccess: (value: any) => {
        setUrl(value[0]?.url);
        setLoading(false);
      },
      onError: () => {
        setLoading(false);
      },
    });
  };
  return (
    <Grid container padding={4}>
      <Grid item xs={6} component="form" onSubmit={handleSubmit(onSubmit)}>
        <Grid item xs={12}>
          <TextArea
            control={control}
            autoComplete="prompt"
            name="prompt"
            placeholder="Prompt..."
            label="Prompt"
            required
            fullWidth
          />
        </Grid>
        <Grid item xs={12} mt={2}>
          <RadioComponent
            fullWidth
            name="size"
            label="Size"
            control={control}
            options={SIZE}
            required
          />
        </Grid>
        <Grid item xs={12} mt={2}>
          <Box sx={{ display: "flex", justifyContent: "flex-start" }}>
            <ButtonCreate type="submit">Generated</ButtonCreate>
          </Box>
        </Grid>
      </Grid>
      <Grid item xs={6}>
        <Box
          sx={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            height: "100%",
          }}
        >
          {loading ? (
            <CircularProgress />
          ) : url ? (
            <img
              style={{
                maxWidth: "650px",
                width: "100%",
                maxHeight: "650px",
                height: "100%",
                objectFit: "cover",
              }}
              src={url}
              alt="Generated Content"
            />
          ) : (
            <Box
              width={"650px"}
              border={"4px solid #8D66F5"}
              height={"650px"}
              maxHeight={"650px"}
              display={"flex"}
              alignItems={"center"}
              justifyContent={"center"}
            >
              <Typography>No Image</Typography>
            </Box>
          )}
        </Box>
      </Grid>
    </Grid>
  );
};

const ButtonCreate = styled(Button)(({ theme }) => ({
  background: theme.palette.base.primary,
  borderColor: theme.palette.base.primary,
  color: theme.palette.base.white,
  height: 44,
  padding: "9px 16px",
  fontSize: "16px",
  fontWeight: 500,
  lineHeight: "20px",
  "&:hover": {
    background: theme.palette.base.primary,
    borderColor: theme.palette.base.primary,
  },
  "&:focus": {
    background: theme.palette.base.primary,
    borderColor: theme.palette.base.primary,
  },
}));

export default DemoScreen;
