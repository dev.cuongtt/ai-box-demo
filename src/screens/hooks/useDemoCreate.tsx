import { useMutation } from "@tanstack/react-query";
import { AxiosError } from "axios";
import { UseFormSetError } from "react-hook-form";
import * as yup from "yup";
import { createDemo } from "../../libs/api/demo";
type TypeOf<T extends yup.AnySchema> =
  T extends yup.Schema<infer U> ? U : never;

export const DemoSchema = yup.object().shape({
  prompt: yup.string().required("Please fill in your prompt."),
  size: yup.string().required("Please fill in your size."),
});

export type Demo = TypeOf<typeof DemoSchema>;

export interface ErrorTypeResponse extends AxiosError {
  errors: {
    [key: string]: string;
  };
}

export const useDemoCreate = (setError: UseFormSetError<Demo>) => {
  const handleMutationError = (error: ErrorTypeResponse) => {
    const errorValidation = error?.errors || {};

    if (errorValidation) {
      Object.entries(errorValidation).forEach(([key, message]) => {
        if (message) {
          setError(key as keyof Demo, { message });
        }
      });
    }
  };

  const mutation = useMutation({
    mutationFn: createDemo,
    onError: handleMutationError,
  });

  return mutation;
};
