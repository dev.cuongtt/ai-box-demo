const base = {
  primary: "#8D66F5",
  primary_pale: "#F5EDED",
  violet: "#8D66F5",
  black: "#2E2E2E",
  white: "#FFFFFF",
  separate_nav: "#D6D6D6",
  separate_contents: "#E6E6E6",
  bg_secondary: "#FAFAFA",
  bg_dark: "#000000A6",
  bg_light: "#FFFFFF80",
  bg_violet: "#F4F1FE",
  grey: "#7E7E7E",
  greyBottom: "#EDECEF",
  bg_btn_cancel: "#ECEBED",
  bg_disabled: "#F9F9F9",
};

const statusColors = {
  assistant: "#0E7DFF",
  success: "#12B13A",
  error: "#D43030",
  warn: "#F4BF00",
  assistant_pale: "#ECF6FE",
  success_pale: "#DFF8E4",
  error_pale: "#FEECEC",
  warn_pale: "#FFF2C6",
  warn_dark: "#D3900C",
  overdue: "#DC0000",
  on_going: "#00D245",
  done: "#8D66F5",
  dismissed: "#7E7E7E",
  pending: "#FFCA00",
};

const activeColors = {
  dark_hover: "#FFFFFF14",
  dark_focus: "#FFFFFF3D",
  dark_pressed: "#FFFFFF52",
  dark_dragged: "#FFFFFF29",
  dark_selected: "#FFFFFF29",
  dark_activated: "#FFFFFF3D",
  light_hover: "#FFFFFF0A",
  light_focus: "#FFFFFF1F",
  light_pressed: "#FFFFFF1F",
  light_dragged: "#FFFFFF14",
  light_selected: "#FFFFFF14",
  light_activated: "#FFFFFF1F",
};

const mono = {
  50: "#EEEEEE",
  200: "#BDBDBD",
  500: "#7A7A7A",
  600: "#494949",
  900: "#151515",
};

export { activeColors, base, mono, statusColors };
