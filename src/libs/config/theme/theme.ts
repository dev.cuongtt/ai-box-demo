import type { Theme } from "@mui/material";
import createTheme from "@mui/material/styles/createTheme";
import { base, mono, statusColors } from "./colors";

declare module "@mui/material" {
  interface Palette {
    base: typeof base;
    status: typeof statusColors;
    mono: typeof mono;
  }

  interface PaletteOptions {
    base: typeof base;
    status: typeof statusColors;
    mono: typeof mono;
  }
}

const defaultTheme: Theme = createTheme({
  palette: {
    common: {
      white: base.bg_light,
      black: mono[500],
    },
    primary: {
      main: base.primary,
      light: base.bg_light,
      contrastText: base.primary,
    },
    grey: {
      900: mono[900],
      600: mono[600],
      500: mono[500],
      200: mono[200],
      50: mono[50],
    },
    success: {
      main: statusColors.success,
    },
    info: {
      main: statusColors.assistant,
    },
    background: {
      default: base.bg_secondary,
    },
    text: {
      primary: base["black"],
      secondary: mono[600],
    },
    base,
    status: statusColors,
    mono,
  },
  typography: {
    h1: {
      fontSize: 24,
      lineHeight: "32.78px",
      fontWeight: 800,
    },
    h2: {
      fontSize: 22,
      lineHeight: "22px",
      fontWeight: 700,
    },
    h3: {
      fontSize: 20,
      lineHeight: "27.32px",
      fontWeight: 800,
    },
    h4: {
      fontSize: 16,
      lineHeight: "16px",
      fontWeight: 700,
    },
    body1: {
      fontSize: 16,
      lineHeight: "21.86px",
      fontWeight: 500,
    },
    body2: {
      fontSize: 14,
      lineHeight: "20px",
      fontWeight: 500,
    },
    subtitle1: {
      fontSize: 12,
      lineHeight: "16px",
      fontWeight: 500,
    },
    caption: {
      fontSize: 11,
      lineHeight: "16px",
      fontWeight: 500,
    },
    fontFamily: ['"Avenir"', "sans-serif"].join(","),
  },
  components: {
    MuiButton: {
      styleOverrides: {
        root: {
          margin: "0px !important",
          height: 44,
          fontSize: 16,
          flexShrink: 0,
          fontWeight: 500,
          borderRadius: 4,
          boxShadow: "none",
          lineHeight: "16px",
          fontStyle: "normal",
          padding: "9px 16px",
          textTransform: "none",
          minWidth: "inherit",
        },
        contained: {
          background: base.violet,
          color: base.white,
          ":hover": {
            backgroundColor: base.violet,
            boxShadow: "none",
          },
          ":focus": {
            backgroundColor: base.violet,
          },
          ":disabled": {
            backgroundColor: base["bg_violet"],
            color: base.white,
          },
        },
        outlined: {
          background: base.white,
          border: `2px solid ${base.violet}`,
          color: base.violet,
          ":hover": {
            background: base.white,
            border: `2px solid ${base.violet}`,
          },
          ":focus": {
            background: base.white,
            border: `2px solid ${base.violet}`,
          },
          ":disabled": {
            color: mono[200],
            borderColor: mono[200],
          },
        },
      },
    },
    MuiChip: {
      styleOverrides: {
        root: {
          padding: "6.5px 8px 6.5px 12px",
          borderColor: base.primary,
          borderRadius: 70,
          height: 24,
          span: {
            lineHeight: "11px",
            fontSize: 11,
            fontWeight: 500,
          },
        },
        label: {
          padding: 0,
        },
        deleteIcon: {
          marginLeft: 4,
          marginRight: 0,
          width: 14,
          justifyContent: "flex-end",
        },
        outlined: {
          backgroundColor: base.bg_light,
          color: base.primary,
          "&:hover": {
            backgroundColor: base.bg_light,
            color: base.primary,
          },
        },
        filled: {
          backgroundColor: base.primary,
          color: base.white,
          "&:hover": {
            backgroundColor: base.primary,
            color: base.white,
          },
        },
      },
    },
    MuiFormLabel: {
      styleOverrides: {
        root: {
          fontSize: 12,
          lineHeight: "16px",
          color: mono[500],
          "&.Mui-focused": {
            color: mono[500],
          },
          "&.Mui-error": {
            color: mono[500],
          },
        },
      },
    },
    MuiFormHelperText: {
      styleOverrides: {
        root: {
          marginLeft: 0,
          fontSize: 12,
          lineHeight: "16px",
          color: statusColors.error,
          marginRight: 0,
        },
      },
    },
    MuiOutlinedInput: {
      styleOverrides: {
        root: {
          background: base.white,
          color: mono[600] + "!important",
          outline: "none",
          paddingRight: 0,
          borderRadius: 4,
          "& .MuiOutlinedInput-input": {
            fontSize: 14,
            lineHeight: "20px",
            fontStyle: "normal",
            height: 16,
            padding: "12px 10px 12px 16px",
            webkitTextFillColor: mono[600],
          },
          "&.MuiOutlinedInput-root": {
            fieldset: {
              borderColor: base.separate_nav,
            },
            "&.Mui-focused fieldset": {
              border: `1px solid ${base.separate_nav}`,
            },
            "&:hover fieldset": {
              border: `1px solid ${base.separate_nav}`,
            },
            "&::placeholder": {
              color: mono[200],
            },
          },
          "&.Mui-error": {
            "&.Mui-focused fieldset": {
              border: `1px solid ${statusColors.error}`,
            },
            "&:hover fieldset": {
              border: `1px solid ${statusColors.error}`,
            },
          },
        },
        adornedStart: {
          paddingLeft: 12,
          "& .MuiInputAdornment-root": {
            marginRight: 0,
          },
          "& .MuiOutlinedInput-input": {
            padding: "12px 10px 12px 0px",
          },
        },
        adornedEnd: {
          "& .MuiOutlinedInput-input": {
            padding: "12px 0px 12px 16px",
          },
        },
      },
    },
    MuiSelect: {
      styleOverrides: {
        root: {
          "& .MuiSelect-icon": {
            top: 9,
          },
          "& .MuiOutlinedInput-input": {
            color: mono[600],
            padding: "0 10px 0 16px",
            fontSize: 14,
            lineHeight: "20px",
          },
        },
      },
    },
    MuiList: {
      styleOverrides: {
        root: {
          "&.MuiList-root": {
            paddingBottom: 0,
            paddingTop: 0,
          },
        },
      },
    },
    MuiTooltip: {
      styleOverrides: {
        tooltip: {
          fontSize: 12,
          lineHeight: "16px",
          color: base.white,
        },
      },
    },
  },
});

defaultTheme.shadows[1] = "0px 2px 11px 0px #3B3C3E2E";

export { defaultTheme };
