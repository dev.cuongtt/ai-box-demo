import { axiosInstance } from "..";
import { Demo } from "../../screens/hooks";

export const createDemo = async (data: Demo) => {
  const response = await axiosInstance.post("chat", {
    ...data,
  });

  return response.data.data;
};
