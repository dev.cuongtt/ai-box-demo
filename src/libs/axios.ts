import axios from "axios";

axios.defaults.withCredentials = false;

const axiosInstance = axios.create();

axiosInstance.defaults.baseURL = import.meta.env.VITE_API_URL;

axiosInstance.defaults.headers.post["Content-Type"] = "application/json";

export { axiosInstance };
