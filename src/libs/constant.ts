const BOX_OPTIONS = [
  {
    label: 'Rectangular',
    value: 1
  },
  {
    label: 'Square',
    value: 2
  },
  {
    label: 'Round',
    value: 3
  }
]

const IS_ENABLED = [
  {
    label: 'Un Active',
    value: 0
  },
  {
    label: 'Active',
    value: 1
  }
]

const BOX_STYLE_OPTIONS = [
  { label: 'Fold', value: 1 },
  { label: 'Lid', value: 2 },
  { label: 'Pullout', value: 3 },
]

const NUMBER_IMAGE_OPTIONS = [
  { label: '1', value: 1 },
  { label: '2', value: 2 },
  { label: '3', value: 3 },
  { label: '4', value: 4 },
]

const SIZE_IMAGE_OPTIONS = [
  { label: '512 x 512', value: 0 },
  { label: '1024 x 1024', value: 1 }
]

const IS_RANDOM = [
  { label: 'Yes', value: 1 },
  { label: 'No', value: 0 }
]

export {
  BOX_OPTIONS,
  BOX_STYLE_OPTIONS,
  NUMBER_IMAGE_OPTIONS,
  SIZE_IMAGE_OPTIONS,
  IS_RANDOM,
  IS_ENABLED
}