import { ThemeProvider } from "@emotion/react";
import { CssBaseline } from "@mui/material";
import { QueryClientProvider } from "@tanstack/react-query";
import { queryClient } from "./libs";
import { defaultTheme } from "./libs/config/theme";
import DemoScreen from "./screens/DemoScreen";

function App() {
  return (
    <>
      <ThemeProvider theme={defaultTheme}>
        <QueryClientProvider client={queryClient}>
          <CssBaseline />
          <DemoScreen />
        </QueryClientProvider>
      </ThemeProvider>
    </>
  );
}

export default App;
